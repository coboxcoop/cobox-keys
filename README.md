# cobox-keys

A simple repository for standardising the way we save keys across the cobox stack, as well as a derivation function that can be used in different space types.

Stores keys as files with permissions restricted to read-only, preventing the software programatically altering the key file once it exists.

```
const crypto = require('cobox-crypto')
const keys = require('cobox-keys')

let keyName = 'encryption_key'
let encryptionKey = keys.loadKey('path/to/key', keyName) || crypto.encryptionKey()

keys.saveKey('path/to/key', keyName, encryptionKey)
```
